# Strategos_Minimap
 Addon for WoW 1.12 (vanilla) 
 
## Current Features
- Independant Battlefield Minimap:
  - Shrinkable
  - Raid/Party members colored by current health
  - Target highlight
  - Raid marks highlights
  - Left click to target
  - Right click to whisp

# FAQs

## How can I move the minimap?

Drag it by the blue cog.

## How can I scale the minimap or its elements?
- Mouse Wheel to scale minimap and all components
- Shif + Mouse Wheel to scale units
- Ctrl + Mouse Wheel to scale point of interest
